## Sanal Ortam (Virtual Environment) Kurulum
```
virtualenv venv
```
## Sanal Ortam Aktif
### Windows
```
venv\Scripts\activate
```
### Linux ve OS X
```
source venv/bin/activate
```
---------------------------------------
## Proje Dizinine Geç
```
cd evreka
```

## Paketleri Yükle
```
pip install -r requirements.txt
```

## Çalıştır
```
python manage.py runserver
```
## /admin => Kullanıcı Adı ve Şifre
```
Kullanıcı adı : evreka
Şifre : 123456
Eposta : admin@evreka.co
```
